package reversi;

abstract class Zaidejas {
	char simbolis='\0';
	String ejikas=null;
	int taskai; // Juodas, Baltas inherits this one
	abstract void printEina(); //stub for Juodas, Baltas polymorphism
	abstract void printLaimejo();
}