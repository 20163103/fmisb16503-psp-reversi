package reversi;

import static org.junit.Assert.*;

import java.lang.reflect.Method;

import org.junit.Test;

public class Testai {

	@Test
	public void testasLentaArRibose() {
		//assertEquals(expected, actual);
		
		Lenta test=new Lenta();
		if(Lenta.LENTOS_ILGIS==8){
			assertEquals(true, test.arRibose(0, 0));
			assertEquals(true, test.arRibose(7, 7));
			assertEquals(false, test.arRibose(-1, 0));
			assertEquals(false, test.arRibose(0, 8));
		}
		else if(Lenta.LENTOS_ILGIS==6){
			assertEquals(true, test.arRibose(0, 0));
			assertEquals(true, test.arRibose(5, 5));
			assertEquals(false, test.arRibose(-1, 0));
			assertEquals(false, test.arRibose(0, 6));
		}
		else if(Lenta.LENTOS_ILGIS==4){
			assertEquals(true, test.arRibose(0, 0));
			assertEquals(true, test.arRibose(3, 3));
			assertEquals(false, test.arRibose(-1, 0));
			assertEquals(false, test.arRibose(0, 4));
		}
		else{
			fail("LENTOS_ILGIS nustatytas ne 4/6/8.");
		}
	}
	
	@Test
	public void testasLentaGautiX() {
		//assertEquals(expected, actual);
		
		Lenta test=new Lenta();
		if(Lenta.LENTOS_ILGIS==8){
			assertEquals(0, test.gautiX(0));
			assertEquals(7, test.gautiX(7));
			assertEquals(1, test.gautiX(9));
		}
		else if(Lenta.LENTOS_ILGIS==6){
			assertEquals(0, test.gautiX(0));
			assertEquals(5, test.gautiX(5));
			assertEquals(1, test.gautiX(7));
		}
		else if(Lenta.LENTOS_ILGIS==4){
			assertEquals(0, test.gautiX(0));
			assertEquals(3, test.gautiX(3));
			assertEquals(1, test.gautiX(5));
		}
		else{
			fail("LENTOS_ILGIS nustatytas ne 4/6/8.");
		}
	}
	
	@Test
	public void testasLentaGautiY() {
		//assertEquals(expected, actual);
		
		Lenta test=new Lenta();
		if(Lenta.LENTOS_ILGIS==8){
			assertEquals(0, test.gautiY(7));
			assertEquals(1, test.gautiY(8));
		}
		else if(Lenta.LENTOS_ILGIS==6){
			assertEquals(0, test.gautiY(5));
			assertEquals(1, test.gautiY(6));
		}
		else if(Lenta.LENTOS_ILGIS==4){
			assertEquals(0, test.gautiY(3));
			assertEquals(1, test.gautiY(4));
		}
		else{
			fail("LENTOS_ILGIS nustatytas ne 4/6/8.");
		}
	}

}
