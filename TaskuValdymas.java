package reversi;

public class TaskuValdymas{
	Lenta lenta;
	TaskuValdymas(Lenta lenta){
		this.lenta=lenta;
	}
	void taskuRadimas(Zaidejas zaidejas){
		zaidejas.taskai=0;
		char charAtPos;
		for(int i=0;i<Lenta.LENTOS_DYDIS;i++){
			charAtPos=lenta.getPositionState(i);
			if(charAtPos==zaidejas.simbolis) zaidejas.taskai++;
		}
	}
	Zaidejas laimetojoNustatymas(Zaidejas zaidejas1, Zaidejas zaidejas2){
		taskuRadimas(zaidejas1);
		taskuRadimas(zaidejas2);
		if(zaidejas1.taskai>zaidejas2.taskai) return zaidejas1;
		if(zaidejas1.taskai<zaidejas2.taskai) return zaidejas2;
		return null;
	}
	void printTaskai(Zaidejas zaidejas1, Zaidejas zaidejas2){
		System.out.println(zaidejas1.taskai+"–"+zaidejas2.taskai);
	}
}