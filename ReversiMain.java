/*
 *  Abstract:
 * Othello, dažniau žinomas kaip reversi
 * tai stalo žaidimas dviems žaidėjams
 * pagal 1971 m. patentuotas taisykles.
 * 
 *  Darbą pateikė:
 * Rokas Rimkevičius
 * PRiF-18/3
 * 20163103
*/

package reversi;

public class ReversiMain {
	public static void main(String[] args){
		//Lentos dydis 8x8, kaip šachmatuose, tai ir koordinačių nurodymus taikysime tuos pačius [A-H][1-8]
		//tik kad reversi othello lenta skaičiai nuo viršaus 1 iki 8!
		// pravers ((int)'RAIDĖ' - 64)
		//Juodi eina pirmi
		//Kiekvienas laukelis lentoje turi vieną iš trijų galimų būsenų
		// vadinsiu 0, J, B => saugosiu kaip char 
		
		//_prototipas:
		//Funkcijos:
		//sukuriamLenta();
		//piesiamLentą(); tekstu
		//'N' -> restart(); restartuoja lentą
		//
		// Klasės:
		//Lenta();
		startGame();
		//jeigu abu žaidėjai negali padaryti ėjimo – žaidimas baigtas
		return;
	}
    static void startGame(){
    	Game game=new Game();
    	while(game.start()) game=new Game();
		  //System.out.print(lenta.getPositionState(5+3*8));
	}
}
