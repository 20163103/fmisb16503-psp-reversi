package reversi;

public class Lenta {
	public static final int LENTOS_ILGIS = 8;
	public static final int LENTOS_DYDIS = LENTOS_ILGIS*LENTOS_ILGIS; //tik lyginės kvadratinės ir nedidinti, nes nenupieš lentos valdymo
	private char[] lenta;
	
	public Lenta(){
		this.lenta= new char[LENTOS_DYDIS];
		pakraunamNulius();
		pakraunamPradzia();
	}
	private void pakraunamNulius(){
		for(int i=0;i<LENTOS_DYDIS;i++){
			this.lenta[i]='0'; //būtų ne char, o int – nereiktų:D
		}
	}
	private void pakraunamPradzia(){
		//00000000 (x3)
		//000BJ000 taip atrodys, kai lentos ilgis 8
		//000JB000
		this.lenta[(LENTOS_DYDIS>>1)-(LENTOS_ILGIS>>1)-1]='B'; //pigus radimas netaikant daugybos (for fun)
		this.lenta[(LENTOS_DYDIS>>1)-(LENTOS_ILGIS>>1)  ]='J'; //čia >>1 tas pats kaip /2
		this.lenta[(LENTOS_DYDIS>>1)+(LENTOS_ILGIS>>1)-1]='J'; //LENTOS_DYDIS/2±LENTOS_ILGIS/2 ir offsetukai
		this.lenta[(LENTOS_DYDIS>>1)+(LENTOS_ILGIS>>1)  ]='B'; //nesvarbu, nes paskaičiuojamas tik kompiliavimo metu 
	}
	public char getPositionState(int idx){
		return this.lenta[idx];
	}
	public void setPositionState(int idx, char state){
		this.lenta[idx]=state;
	}
	public boolean arRibose(int x, int y){
		if(x>=0 && x<Lenta.LENTOS_ILGIS && y>=0 && y<Lenta.LENTOS_ILGIS) return true;
		return false;
	}
	public int gautiX(int i){
		return i%Lenta.LENTOS_ILGIS;
	}
	public int gautiY(int i){
		return i/Lenta.LENTOS_ILGIS;
	}
}
