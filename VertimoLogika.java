package reversi;

public class VertimoLogika{
	Game game;
	Lenta lenta;
	public VertimoLogika(Game game, Lenta lenta){
		this.game=game;
		this.lenta=lenta;
	}
	boolean apversti(int x, int y, int kx, int ky, boolean arSimuliacija){
		int apverciami=0;
		x+=kx;
		y+=ky;
		while(lenta.arRibose(x, y)){
			char esamaPozicija=lenta.getPositionState(x+y*Lenta.LENTOS_ILGIS);
			if(apverciami==0 && esamaPozicija!=game.laukia.simbolis) return false;
			if(esamaPozicija==game.laukia.simbolis) apverciami++;
			else if(esamaPozicija=='0')	return false;
			else if(arSimuliacija) return true;
			else{ //mission success
				apverstiRasymas(apverciami, x, y, kx, ky);
				return true;
			}
			x+=kx;
			y+=ky;
		}
		return false; //pasiekiama, kai beieškant patenkama už lentos
	}
	void apversti(int x, int y, int kx, int ky){ //kad NE simuliacijos atveju nereiktų papildomo argumento
		apversti(x, y, kx, ky, false);
	}
	void apverstiRasymas(int apverciami, int x, int y, int kx, int ky){
		for(int i=0;i<apverciami;i++){
			x-=kx;
			y-=ky;
			lenta.setPositionState(x+y*Lenta.LENTOS_ILGIS, game.eina.simbolis);
		}
	}
}
