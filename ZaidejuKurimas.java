package reversi;

public class ZaidejuKurimas {
	Zaidejas sukurtiZaideja(String zaidejoSpalva){
		if(zaidejoSpalva=="juodas") return new Juodas(); //polimorfizmas, nes Zaidejas
		if(zaidejoSpalva=="baltas") return new Baltas(); //polimorfizmas, nes Zaidejas
		return null;
	}
}
