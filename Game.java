package reversi;

import java.util.Scanner;

public class Game {
	Zaidejas eina;
	Zaidejas laukia;
	int praleistiEjimai;
	Lenta lenta;
	LentosSpausdinimas lentosSpausdinimas;
	DetalesDejimas detalesDejimas;
	TaskuValdymas taskuValdymas;
	Zaidejas juodas;
	Zaidejas baltas;
	public Game(){
		this.lentosSpausdinimas=new LentosSpausdinimas();
		this.lenta=new Lenta();
		this.detalesDejimas=new DetalesDejimas(this, this.lenta);
		this.taskuValdymas=new TaskuValdymas(this.lenta);
		this.juodas=new ZaidejuKurimas().sukurtiZaideja("juodas"); //creational design pattern (factory)
		this.baltas=new ZaidejuKurimas().sukurtiZaideja("baltas"); //creational design pattern (factory)
		this.eina=this.juodas;
		this.laukia=this.baltas;
		this.praleistiEjimai=0;
	}
	void flipTurn(){
		Zaidejas tmp=eina;
		eina=laukia;
		laukia=tmp;
	}
	void endGame(){
		lentosSpausdinimas.spausdintiLenta(lenta);
		Zaidejas laimetojas=taskuValdymas.laimetojoNustatymas(juodas, baltas);
		if(laimetojas==null) System.out.print("Lygiosios. ");//darytas null tikrinimas, nes kaip
//Zaidejas abstrakcija neduoda sukurti default one, kuris tiko Lygiosios spausdinimui
		else laimetojas.printLaimejo(); //pritaikytas polimorfizmas, nereikia rašyti if ifelse else tikrinimų
		taskuValdymas.printTaskai(juodas, baltas);
		eina=null;
	}
	boolean start(){
		while(ejimas()); //eiti kol yra ėjimų
		System.out.println("Ar žaisti dar kartą? (T/N): ");
		Scanner sc = new Scanner(System.in);
	    String ivestis = sc.nextLine().toUpperCase();
		if(ivestis.charAt(0)=='T') return true;
		System.out.println("Viso gero.");
		return false;
	}
	boolean ejimas(){
		if(detalesDejimas.arYraKurPadeti()){
			lentosSpausdinimas.spausdintiLenta(lenta);
			spausdintiEjika();
			while(!detalesDejimas.padetiDetale()); //tegul įveda ką reikia
		}
		else if(!praleidziamasEjimas()) return false; //praleidziamasEjimas() praneša apie užbaigtą partiją
		flipTurn();
		return true;
	}
	void spausdintiEjika(){
		eina.printEina();
	}
	boolean praleidziamasEjimas(){
		praleistiEjimai++;
		if(praleistiEjimai>=2){
			endGame();
			return false; //praleidziamasEjimas praneša apie pabaigtą partiją
		}
		System.out.println("Kadangi ejikas "+eina.ejikas+" neturi kur padėti, praleidžia ėjimą.");
		return true; //grąžinama, kad sėkmingai praleistas ėjimas
	}
}
