package reversi;

public class LentosSpausdinimas {
	void spausdintiLenta(Lenta lenta){
		spausdintiKoordRaides();
		for(int i=0;i<Lenta.LENTOS_ILGIS;i++){
			spausdintiNumeri(i);
			spausdintiLentosEilute(lenta, i);
		}
		System.out.println();
	}
	void spausdintiKoordRaides(){ //dinaminis System.out.println("   A B C D E F G H\n"); variantas
		String raidziuKoord="   ";
		char raide='A';
		for(int i=0;i<Lenta.LENTOS_ILGIS;i++){
			raidziuKoord+=" "+raide;
			raide++;
		}
		System.out.println(raidziuKoord+"\n");
	}
	void spausdintiNumeri(int i){
		System.out.print(i+1+"   ");
	}
	void spausdintiLentosEilute(Lenta lenta, int i){
		for(int j=0;j<Lenta.LENTOS_ILGIS;j++){
			System.out.print(lenta.getPositionState(j+i*Lenta.LENTOS_ILGIS)+" ");
		}
		System.out.println();
	}
}
