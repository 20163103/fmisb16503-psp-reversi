package reversi;

import java.util.Scanner;

public class DetalesDejimas{
	private static final boolean SIMULIACIJA = true; //reikšmė kaip angl. pretend
	private static final int[] KX={-1,1, 0,0,-1,-1, 1, 1}; //kryptys kairė, dešinė, apačia, viršus, ka, kv, dv, da
	private static final int[] KY={ 0,0,-1,1,-1, 1, 1,-1};
	Game game;
	Lenta lenta;
	VertimoLogika vertimoLogika;
	public DetalesDejimas(Game game, Lenta lenta){
		this.vertimoLogika=new VertimoLogika(game, lenta);
		this.game=game;
		this.lenta=lenta;
	}
	boolean arYraKurPadeti(){
		for(int i=0;i<Lenta.LENTOS_DYDIS;i++){
			if(arGaliuPadeti(i)) return true;
		}
		return false;
	}
	boolean padetiDetale(){
		game.praleistiEjimai=0;
		System.out.print("Ivesk koordinates: ");
		Scanner sc = new Scanner(System.in); //read input
	    String ivestis = sc.nextLine().toUpperCase();
		int x=(int)ivestis.charAt(0)-65; //ascii 'A' -> 0
		int y=(int)ivestis.charAt(1)-49; //ascii '0' -> 0
		//System.out.println(y);
		if(!lenta.arRibose(x, y)){
			System.out.println("Negerai įvedei. Pvz.: F5.");
			return false;
		}
		if(pavykoApversti(x+y*Lenta.LENTOS_ILGIS)==false){
			//pranesti, kad netinkamas pasirinkimas
			System.out.println("Čia nepasideda.");
			return false;
		}
		lenta.setPositionState(x+y*Lenta.LENTOS_ILGIS, game.eina.simbolis);
		return true;
	}
	boolean arGaliuPadeti(int i){
		if(lenta.getPositionState(i)!='0') return false;
		int x=lenta.gautiX(i);
		int y=lenta.gautiY(i);
		for(int k=0;k<KX.length;k++){ // 8 kryptys
			if(vertimoLogika.apversti(x, y, KX[k], KY[k], SIMULIACIJA)) return true;
		}
		return false;
	}
	boolean pavykoApversti(int i){
		if(!arGaliuPadeti(i)) return false; //jeigu nėra ką apversti – return false
		int x=lenta.gautiX(i);
		int y=lenta.gautiY(i);
		for(int k=0;k<KX.length;k++){ // 8 kryptys
			vertimoLogika.apversti(x, y, KX[k], KY[k]);
		}
		return true;
	}
}
